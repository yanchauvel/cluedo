#! /usr/bin/python3

import socket
import sys
import random

nom = "IA"
port = 1234
host = "localhost"

carte = []
hypothese = []
secret = []
vue = []

# Connexion
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((host, port))
so = socket.makefile('rw')

# Recup des données
demand = so.readline().split()
N = int(demand[2])

# Dit bonjour
socket.send(("B "+nom).encode())

# Acusation valide
def AccBot0():
    socket.send(("A " + str(0) + " " + str(N+1) + " " + str(2*N+1)).encode())

def Tour():
    AccBot0()

def Montrer():
    # Nous donne une liste des cartes en commun
    same = list(set(carte)&set(hypothese))

    # On renvoie la premiere carte qu'il trouve en commun entre hypothese et nos cartes
    socket.send(("M "+str(same[0])).encode())

while( True ):
    demand = so.readline().split()

    if(len(demand) == 0):
        continue

    match demand[0]:

        # C'est ton tour
        case "T":
            Tour()

        # Un autre joueur à fait une hypothese
        case "H":
            hypothese = list(map(int, demand[1:]))
            demand = so.readline()
            match demand[0]:
                # Tu dois montrer une carte
                case "C":
                    Montrer()
                # Un joueur n'a pas pu montrer de carte
                case "P":
                    demand = so.readline().strip().split(" ")
                    print("Le joueur numéro " + str(demand[1]) + "a montrer une carte")
                # Un joueur à montrer une carte
                case "M":
                    if(len(demand) == 1):
                        print("L'hypothèse est possiblement la solution")
                    else:
                        print("Le joueur numéro " + str(demand[1]) + "a montrer une carte")

        # Voici tes cartes
        case "C":
            carte = list(map(int, demand[1:]))
        
        # Ce joueur n'a pas pu te montrer une de ses cartes
        case "P":
            print("Le joueur " + str(demand[1]) + " n'a pas montrer de carte")

        # Réponse à notre hypothèse
        case "M":
            if(len(demand) == 1):
                print("L'hypothèse est possiblement la solution")
            if(len(demand) == 3):
                print("Le joueur numéro " + str(demand[1]) + "a montrer la carte " + str(demand[2]))


