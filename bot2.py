#! /usr/bin/python3

import socket
import sys
import random
import time


class Logique:

    #### Class logique comporte un dictionnaire M séparer en deux sous dictionnaire "M" et "-M"
    ## Le sous dictionnaire "M" permet de stocker les littéraux de possetion de carte d'un joueur (le joueur étant la clée et la valeur une liste de litéraux
    ## Le sous dictionnaire "-M" permet de stocker les littéraux de manque càd si un joueur ne possède pas une carte, sous la forme d'une liste d'entier correspondant aux cartes
    ## La classe possède également une liste vide S représantant le secret et se rempliera au fur et à mesure de la connaissance obtenue des carte adverses
    
    def __init__(self, numPlayer):
        a = {}
        b = {}
        for i in range(numPlayer):
            a[i] = []
            b[i] = []
        
        self.M = {"M": a, "-M": b}
        self.S = []
        self.numPlayer = numPlayer

    ####
    #### Fonction permettant d'ajouter des litéraux de possetion par exemple si un joueur répond à une hypothèse
    ####

    def ajoutPossesion(self, joueur, carte):

        # On ajoute seulement les clause de cartes n'étant pas déjà présante dans la liste du sous dictionnaire "M", on retire de la clause les cartes que le joueur ne possède pas
        
        if carte not in self.M["M"][joueur]:
            for i in self.M["-M"][joueur]:
                if i in carte:
                    carte.remove(i)

            # Si la clause ajouter ne comporte qu'un litéral, alors on ajoute à tous les autres joueurs la clause -M

            if len(carte) == 1:
                self.M["M"][joueur].append(carte)
                for player in range(self.numPlayer):
                    if player != joueur:
                        self.ajoutManque(player,min(carte))
                self.verificationJoueur()

            # Sinon on ajoute la clause au sous dictionnaire "M"
            
            if len(carte) > 1:
                self.M["M"][joueur].append(carte)

    ####
    #### Fonction permettant d'ajouter des litéraux de manque par exemple si un joueur n'a pas répondu à une hypothèse
    ####

    def ajoutManque(self, joueur, carte):

        # On ajoute seulement les clause de cartes n'étant pas déjà présante dans la liste du sous dictionnaire "-M"
        
        if carte not in self.M["-M"][joueur]:
            self.M["-M"][joueur].append(carte)
            self.equilibrageLitéraux(joueur,carte)
        


    ####
    #### Fonction permettant les réductions de litéraux au fur et à mesure de nos connaissance certaines (tableau -M)
    ####

    def equilibrageLitéraux(self,joueur,M):

        # Si on trouve une clause comportant la carte M alors on la retire de la clause, si la clause devient une clause unitaire alors on lance différant équilibrage
        
        for literal in self.M["M"][joueur]:
            if M in literal:
                literal.remove(M)
                if(len(literal) == 1):
                    self.equilibreJoueur(joueur,min(literal))
                    self.verificationJoueur()
        
        """
        for literal in self.M["M"][joueur]:
            if len(literal) == 1:
                if min(literal) in c1:
                    for i in c1:
                        if(i != min(literal)):
                            self.ajoutManque(joueur,i)
                if min(literal) in c2:
                    for i in c2:
                        if(i != min(literal)):
                            self.ajoutManque(joueur,i)
                if min(literal) in c3:
                    for i in c3:
                        if(i != min(literal)):
                            self.ajoutManque(joueur,i)
        """

                    
    ####
    #### Fonction qui permet ajouter à tout joueur autre que celui passer en argumant la valeur M dans leurs tableau -M (permet de dire que si un joueur possède une carte, aucun autre ne la possède)
    ####


    def equilibreJoueur(self,joueur,M):
        for player in range(self.numPlayer):
            if player == joueur:
                continue
            self.ajoutManque(player,M)

    ####
    #### Fonction qui permet de suprimer les litéraux inutiles dans le cas ou une clause est incluse dans une autre
    ####

    def simplification(self):
        for player in range(self.numPlayer):
            simplified_literals = []

            # Si on trouve un autre litéral qui est inclue dans le premier alors on suprime ce premier de la liste
            
            for literal in self.M["M"][player]:
                is_included = False
                for other_literal in self.M["M"][player]:
                    if literal != other_literal and other_literal.issubset(literal):
                        is_included = True
                        break
                if not is_included:
                    simplified_literals.append(literal)
            self.M["M"][player] = simplified_literals
                            

    ####
    #### Fonction qui vérifie si il est impossible pour tout les joueurs de posséder une carte et l'ajoute au secret
    ####

    def verificationSecret(self):
        secret = list(range(0,3*self.numPlayer+3))
        for player in range(self.numPlayer):
            secret = [value for value in secret if value in self.M["-M"][player]]
        self.S = secret

    ####
    #### Fonction qui vérifie si nous avons déjà la connaissance des trois cartes d'un joueur, si c'est le cas mes à jour son sous dictionnaire -M
    ####

    def verificationJoueur(self):
        for player in range(self.numPlayer):
            carteJoueur = []
            for literal in self.M["M"][player]:
                if len(literal) == 1:
                    if min(literal) not in carteJoueur:
                        carteJoueur.append(min(literal))
            if len(carteJoueur) == 3:
                self.M["-M"][player] = [value for value in list(range(0,3*self.numPlayer+3)) if value not in carteJoueur]


    ####
    #### Fonction d'affichage des valeurs de dictionnaire M et S (donc l'état de la classe Logique)
    ####

    def __str__(self):
         return f"Connaissance : {self.M} \nSecret : {self.S}"


    ####
    #### Fonction qui permet de trouver une Hypothèse valide permettant de gagner de l'information (pertinante)
    ####

    def getHypothese(self, player):
        # Les cartes potentielles possédées par le joueur
        cartes_possibles = self.M["M"][player]

        # Les cartes dont nous sommes certains que le joueur ne les à pas
        cartes_manquantes = self.M["-M"][player]

        # Les cartes dont nous sommes certains que le joueur les possèdent
        carteJ = []
        for clause in cartes_possibles:
            if len(clause) == 1:
                carteJ.append(min(clause))
        carteJoueur = sorted(carteJ)

        # Reste des cartes pertinante à donner en Hypothèse (on ne sait ni si le joueur les possède ou non)

        rest = list(tout - set(cartes_manquantes))
        reste = list(set(rest) - set(carteJoueur))
        c1 = [reste[i] for i in range(len(reste)) if reste[i] >= 0 and reste[i] < self.numPlayer+1]
        c2 = [reste[i] for i in range(len(reste)) if reste[i] >= self.numPlayer+1 and reste[i] <= 2*self.numPlayer+1]
        c3 = [reste[i] for i in range(len(reste)) if reste[i] > 2*self.numPlayer+1]

        # Si il n'existe pas de carte pertinante alors on utilise une carte dont nous sommes sûr que le joueur ne la possède pas
        
        if len(c1) == 0:
            for value in cartes_manquantes:
                if(value>=0 and value <=self.numPlayer+1):
                    c1=[value]
        if len(c2) == 0:
            for value in cartes_manquantes:
                if(value>=self.numPlayer+1 and value <=2*self.numPlayer+1):
                    c2 =[value]
        if len(c3) == 0:
            for value in cartes_manquantes:
                if(value>2*self.numPlayer+1):
                    c3=[value]

        # S'il existe une clause de 3 cartes, on l'utilise
        for clause in cartes_possibles:
            if len(clause) == 3:
                hypothese = [min(clause),random.choice(c2),max(clause)]
                return sorted(hypothese)

        # S'il existe une clause de 2 cartes, on l'utilise
        for clause in cartes_possibles:
            if len(clause) == 2:
                # On prend une des deux cartes de la clause et deux autres pertinante
                if min(clause) <= self.numPlayer:
                    hypothese = [min(clause),random.choice(c2),random.choice(c3)]
                else:
                    hypothese = [random.choice(c1),min(clause),random.choice(c3)]
                return sorted(hypothese)

        # Si aucune des conditions ci-dessus n'est satisfaite, on retourne une clause aléatoire

        hypothese = [random.choice(c1),random.choice(c2),random.choice(c3)]
        return list(hypothese)


nom = "IA"
port = 1235
host = "localhost"

cartes = []
hypothese = []

# Connexion
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((host, port))
so = socket.makefile('rw')

# Recup des données
demand = so.readline().split()
numeroJoueur = int(demand[1])
numPlayer = int(demand[2])
nextPlayer = (numeroJoueur + 1) % numPlayer

# Dit bonjour
socket.send(("B "+nom).encode())
global tout,c1,c2,c3
tout = set(list(range(0,3*numPlayer+3)))
c1 = [list(tout)[i] for i in range(len(tout)) if list(tout)[i] >= 0 and list(tout)[i] < numPlayer+1]
c2 = [list(tout)[i] for i in range(len(tout)) if list(tout)[i] >= numPlayer+1 and list(tout)[i] <= 2*numPlayer+1]
c3 = [list(tout)[i] for i in range(len(tout)) if list(tout)[i] > 2*numPlayer+1]

# Cree la classe Logique
L = Logique(numPlayer)

# Fait une accusation avec notre variable de secret
def Accusation():
    socket.send(("A " + str(L.S[0]) + " " + str(L.S[1]) + " " + str(L.S[2])).encode())

# Fait une Hypothèse pertinante valide
def Hypothese():
    hypothese = L.getHypothese(nextPlayer)
    socket.send(("H " + str(hypothese[0]) + " " + str(hypothese[1]) + " " + str(hypothese[2])).encode())

# Si on a trouver le secret, on fait un accusation sinon on fait une hypothèse
def Tour():
    if len(L.S) == 3:
        Accusation()
    else:
        Hypothese()

def Montrer():
    
    #Nous donne une liste des cartes en commun
    same = list(set(cartes)&set(hypothese))
    
    #On renvoie la premiere qu'il trouve en commun entre hypothese et nos carte
    socket.send(("M "+str(same[0])).encode())

HypotheseSerie = False

while( True ):
    L.verificationSecret()
    demand = so.readline().strip().split(" ")

    match demand[0]:

        # C'est ton tour
        case "T":
            Tour()

        # Un autre joueur à fait une hypothese
        case "H":
            hypothese = list(map(int, demand[1:]))
            demand = so.readline().strip().split(" ")
            match demand[0]:
                # Tu dois montrer une carte
                case "C":
                    Montrer()
                # Un joueur n'a pas pu montrer de carte
                case "P":
                    player = int(demand[1])
                    for carte in hypothese:
                        L.ajoutManque(player, carte)
                    print("Le joueur numéro " + str(player) + "n'a pas montrer une carte")
                
                # Un joueur à montrer une carte
                case "M":
                    if(len(demand) == 1):
                        print("L'hypothèse est possiblement la solution")
                    else:
                        player = int(demand[1])
                        print("Le joueur numéro " + str(player) + "a montrer une carte")
                        L.ajoutPossesion(player, set(hypothese))


        # Voici tes cartes
        case "C":

            # La variable HypotheseSerie permet de gerer le cas ou si on joue avec plus de 2 joueurs, plusieurs joueurs peuvent passer sur une hypothèse avant notre tour
            if (HypotheseSerie):
                Montrer()
            else:
                cartes = list(map(int, demand[1:]))
                for carte in cartes:
                    L.ajoutPossesion(numeroJoueur, {carte})
                HypotheseSerie = True
                
        
        # Ce joueur n'a pas pu te montrer une de ses cartes
        case "P":
            player = int(demand[1])
            for carte in hypothese:
                L.ajoutManque(player, carte)

        # Réponse à notre hypothèse
        case "M":
            if(len(demand) == 1):
                # Personne n'a répondu à l'hypothèse
                print("Aucun joueur n'a répondu à l'hypothèse")
            if(len(demand) == 2):
                # Le joueur à montrer une carte de l'hypothèse
                player = int(demand[1])
                L.ajoutPossesion(player,set(hypothese))
            
            if(len(demand) == 3):
                # Le joueur nous à montrer une carte de l'hypothèse
                player = int(demand[1])
                carte = demand[2]
                L.ajoutPossesion(player,{int(carte)})


