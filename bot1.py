#! /usr/bin/python3


import socket
import sys
import random

nom = "IA"
port = 1234
host = "localhost"

carte = []
hypothese = []
secret = []
vue = []

# Connexion
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((host, port))
so = socket.makefile('rw')

# Recup des données
demand = so.readline().split()
N = int(demand[2])

# Dit bonjour
socket.send(("B "+nom).encode())

# Mets à jour sa liste de cartes connue
def Vue(carte):
    vue.extend(y for y in carte if y not in vue)

def Accusation():
    socket.send(("A " + str(secret[0]) + " " + str(secret[1]) + " " + str(secret[2])).encode())

def Hypothese():
    global hypothese
    tout = set(list(range(0,3*N+3)))
    reste = list(tout - set(vue))

    # Fait la liste des lieu/armes/personnage possibles que l'on a pas déjà vue
    c1 = [reste[i] for i in range(len(reste)) if reste[i] >= 0 and reste[i] < N+1]
    c2 = [reste[i] for i in range(len(reste)) if reste[i] >= N+1 and reste[i] <= 2*N+1]
    c3 = [reste[i] for i in range(len(reste)) if reste[i] > 2*N+1]

    socket.send(("H " + str(c1[0]) + " " + str(c2[0]) + " " + str(c3[0])).encode())
    hypothese = [c1[0],c2[0],c3[0]]


def Tour():
    if len(secret) == 3:
        Accusation()
    else:
        Hypothese()

def Montrer():
    # Nous donne une liste des cartes en commun
    same = list(set(carte)&set(hypothese))

    # On renvoie la premiere carte qu'il trouve en commun entre hypothese et nos cartes
    socket.send(("M "+str(same[0])).encode())

while( True ):
    demand = so.readline().split()

    if(len(demand) == 0):
        continue

    match demand[0]:

        # C'est ton tour
        case "T":
            Tour()

        # Un autre joueur à fait une hypothese
        case "H":
            hypothese = list(map(int, demand[1:]))
            demand = so.readline()
            match demand[0]:
                # Tu dois montrer une carte
                case "C":
                    Montrer()
                # Un joueur n'a pas pu montrer de carte
                case "P":
                    demand = so.readline().strip().split(" ")
                    print("Le joueur numéro " + demand[1] + "n'a pas montrer une carte")
                # Un joueur à montrer une carte
                case "M":
                    if(len(demand) == 1):
                        print("L'hypothèse est possiblement la solution")
                    else:
                        print("Le joueur numéro " + demand[1] + "a montrer une carte")

        # Voici tes cartes
        case "C":
            carte = list(map(int, demand[1:]))
            Vue(carte)
        
        # Ce joueur n'a pas pu te montrer une de ses cartes
        case "P":
            print("Le joueur " + demand[1] + " n'a pas montrer de carte")

        # Réponse à notre hypothèse
        case "M":

            # Ne marche que en 1v1
            if(len(demand) == 1):
                secret = hypothese
            if(len(demand) == 3):
                Vue([int(demand[2])])


